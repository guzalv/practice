#!/usr/bin/env python

class Solution(object):
    def twoSumBruteForce(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        for outer_index in range(len(nums)):
            for inner_index in range(outer_index + 1, len(nums)):
                #print('Outer index: %d' % outer_index)
                #print('Inner index: %d' % inner_index)
                #print('Sum: %s' % str(nums[outer_index] + nums[inner_index]))
                if nums[outer_index] + nums[inner_index] == target:
                    return [outer_index, inner_index]


    def twoSumHashTable(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        nums_hashed = ((index, nums[index]) for i in range(len()))
        for outer_index in range(len(nums)):
            for inner_index in range(outer_index + 1, len(nums)):
                #print('Outer index: %d' % outer_index)
                #print('Inner index: %d' % inner_index)
                #print('Sum: %s' % str(nums[outer_index] + nums[inner_index]))
                if nums[outer_index] + nums[inner_index] == target:
                    return [outer_index, inner_index]
