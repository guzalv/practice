#!/usr/bin/env python

import random

import two_sum

solution = two_sum.Solution()

def test_simple_increasing_integers():
    result_brute_force = solution.twoSumBruteForce([1, 3, 4, 6], 9)
    assert result_brute_force == [1, 3] or result_brute_force == [3, 1]


def test_simple_decreasing_integers():
    result_brute_force = solution.twoSumBruteForce([9, 5, 4, 2], 7)
    assert result_brute_force == [1, 3] or result_brute_force == [3, 1]


def test_result_is_first_and_last_integers():
    result_brute_force = solution.twoSumBruteForce([1, 3, 4, 9], 10)
    assert result_brute_force == [0, 3] or result_brute_force == [3, 0]


def test_array_has_negative_number():
    result_brute_force = solution.twoSumBruteForce([-1, -3, 4, 9], 8)
    assert result_brute_force == [0, 3] or result_brute_force == [3, 0]


def test_array_has_decimal_number():
    result_brute_force = solution.twoSumBruteForce([-1, -3, 4.5, 9], 8)
    assert result_brute_force == [0, 3] or result_brute_force == [3, 0]


def test_result_includes_decimal_nunmber():
    result_brute_force = solution.twoSumBruteForce([-1, -3, 4.5, 9], 13.5)
    assert result_brute_force == [2, 3] or result_brute_force == [3, 2]


def test_all_combinations_are_solved_correctly():
    problem_array = [-1, -3, 4.5, 9]

    for outer_index in range(len(problem_array)):

        for inner_index in range(len(problem_array)):
            if inner_index == outer_index:
                continue

            sum = problem_array[outer_index] + problem_array[inner_index]
            expected_solution = [outer_index, inner_index]
            result_brute_force = solution.twoSumBruteForce(problem_array, sum)

            print('Expected solution: %s' % str(expected_solution))
            print('Result: %s' % str(result_brute_force))

            assert (expected_solution == result_brute_force) or \
                   (expected_solution == list(reversed(result_brute_force)))


def test_random_combination_is_solved_correctly():
    # Generate a list of random numbers
    min_value = -1000000
    max_value = 1000000
    number_of_elements = 10000

    nums = random.sample(range(min_value, max_value),
                         number_of_elements)

    # Generate random indexes of this list. If they are equal, repeat
    first_index, second_index = [random.randint(0, number_of_elements),
                                 random.randint(0, number_of_elements)]
    while first_index == second_index:
        first_index, second_index = [random.randint(0, number_of_elements),
                                     random.randint(0, number_of_elements)]

    # Now we have the indexes and the target, pass them to our function
    target = nums[first_index] + nums[second_index]
    expected_solution = [first_index, second_index]
    result_brute_force = solution.twoSumBruteForce(nums, target)
    sum_of_result = nums[result_brute_force[0]] + nums[result_brute_force[1]]

    print('Target: %d' % target)
    print('Certain solution: %s (%d + %d = %d)' % (
        str(expected_solution),
        nums[first_index],
        nums[second_index],
        target))
    print('Calculated solution: %s (%d + %d = %d)' % (
        str(result_brute_force),
        nums[result_brute_force[0]],
        nums[result_brute_force[1]],
        sum_of_result))

    # Check if it worked
    assert sum_of_result == target


def no_test_array_has_one_item():

    result_brute_force = solution.twoSumBruteForce([3], 3)

    assert result_brute_force == [0, 0]
