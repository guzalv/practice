#/usr/bin/env bash

set -euo pipefail

DOCKER_IMAGE=openjdk:15

java_file=${1:-}

if [ -z "${java_file}" ]; then
    echo "ERROR: Pass java file as first argument"
    exit 1
fi

if ! [ -f "${java_file}" ]; then
    echo "ERROR: Passed file \"${java_file}\" does not exist or can't be read"
    exit 1
fi

directory=$(pwd -P)/$(dirname "${java_file}")
file_name=$(basename "${java_file}")
class_name=${file_name%.java}


docker run \
    --rm \
    --volume "${directory}:/app" \
    "${DOCKER_IMAGE}" sh -c "
        javac /app/${file_name}
        java --class-path /app ${class_name}"
