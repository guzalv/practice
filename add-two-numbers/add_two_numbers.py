# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """

        # Create the result linked list
        l_result = ListNode(0)

        # Init loop variables
        current_node = l_result
        remainder = 0

        # While we still have digits in at least one list, add them up
        while l1 or l2:

            # Temporary variables for convenience
            digit_1 = l1.val if l1 else 0
            digit_2 = l2.val if l2 else 0

            # Add
            digit_result = digit_1 + digit_2 + remainder

            # If result is > 10, get the remainder
            remainder = int(digit_result / 10)

            # Store in current node
            current_node.val = digit_result % 10

            # Advance one in each input list if possible
            l1 = l1.next if l1 else None
            l2 = l2.next if l2 else None

            # Advance one in the result list if there are still digits to
            # process or we have a remainder
            current_node.next = ListNode(0) if (l1 or l2 or remainder) else None
            current_node = current_node.next

        # If there was a remainder, add it still
        if remainder:
            current_node.val = remainder

        return l_result
