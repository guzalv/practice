import add_two_numbers

solution = add_two_numbers.Solution()

def linked_lists_are_equal(l1, l2):
    while True:
        if not l1.val == l2.val:
            return False

        if (not l1.next) and (not l2.next):
            # Values differ
            break

        if (not l1.next) or (not l2.next):
            # Lists don't have same length
            return False

        l1 = l1.next
        l2 = l2.next

    return True


def int_to_linked_list(number):
    '''Convert integer to linked list of digits'''

    result = add_two_numbers.ListNode(-1)

    digit = result

    while True:
        digit.val = number % 10
        if number <= 10:
            break

        number = int(number / 10)
        digit.next = add_two_numbers.ListNode(-1)
        digit = digit.next

    return result


def print_linked_list(l):
    number = []
    while l:
        number.insert(0, l.val)
        l = l.next

    print(number)


def print_debug_info(number_1, number_2, result):
    print('First number: %d' % number_1)
    print('Second number: %d' % number_2)
    print('Expected result:')
    print_linked_list(int_to_linked_list(number_1 + number_2))
    print('Calculated result:')
    print_linked_list(result)


def check_solution(number_1, number_2):
    result = solution.addTwoNumbers(int_to_linked_list(number_1),
                                    int_to_linked_list(number_2))

    print_debug_info(number_1, number_2, result)
    assert linked_lists_are_equal(result,
                                  int_to_linked_list(number_1 + number_2))


def test_operands_and_result_have_same_amount_of_digits():
    check_solution(147, 299)


def test_result_has_more_digits_than_operands():
    check_solution(847, 299)

def test_operands_have_different_amount_of_digits():
    check_solution(847, 292342349)
