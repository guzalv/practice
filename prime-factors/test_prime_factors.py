import pytest

import prime_factors

def test_prime_factors_calculated_correctly_simple_case():
    number = 15
    expected_solution = [3, 5]
    actual_solution = prime_factors.get_prime_factors(number)

    assert expected_solution == actual_solution

def test_prime_factors_calculated_correctly_prime_number():
    number = 7
    expected_solution = [7]
    actual_solution = prime_factors.get_prime_factors(number)

    assert expected_solution == actual_solution

def test_prime_factors_refuses_negative_number():
    number = -15
    with pytest.raises(AssertionError):
        prime_factors.get_prime_factors(number)
