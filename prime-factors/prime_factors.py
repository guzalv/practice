def get_lowest_factor(number):
    for potential_factor in range(2, number + 1):
        if not number % potential_factor:
            return potential_factor

def get_prime_factors(number):
    assert(number  > 0)
    factors = []
    while True:
        lowest_factor = get_lowest_factor(number)
        if not lowest_factor:
            lowest_factor = number
        factors.append(lowest_factor)
        number //= lowest_factor
        if number == 1:
            break

    return factors
