def solve(target, numbers, candidates=[]):
    print("New iteration. Target: %d. Candidates: " % target)
    print(candidates)
    # If this is the first call, all numbers are candidates
    if not candidates:
        candidates = list(range(len(numbers)))

    new_candidates = []

    # Loop through the numbers that we have been told can be a part of the
    # solution (candidates)
    for i in list(range(len(candidates))):
        current_number = numbers[candidates[i]]
        print("Current num: %d" % current_number)

        # If the current number is greater than the target, it cannot be a
        # solution
        if current_number > target:
            continue
        # If the current number matches the target, we are done
        if current_number == target:
            print("Found single number!")
            return [candidates[i]]

        # Otherwise add its index to the new candidates list
        new_candidates.append(candidates[i])

    print("Didnt find single number")
    # If we only had one candidate, and we have reached this point, we return already
    if len(candidates) == 1:
        print("There was only one candidate != target")
        return

    # Call this function again, assuming that one of the candidates is already
    # part of the solution
    for i in list(range(len(new_candidates))):
        print("Tryign with index %d, nnumber %d" % (new_candidates[i], numbers[new_candidates[i]]))
        new_target = target - numbers[new_candidates[i]]
        potential_candidates = new_candidates[:]
        del potential_candidates[i]
        print("New candidates:")
        print(new_candidates)
        print("Potential candidates:")
        print(potential_candidates)
        result = solve(new_target, numbers, potential_candidates)
        if result:
            return [new_candidates[i]] + result

    """
    numbers = [4, 34, 5, 1, 3, 6]
    target = 8
    """

def ssolve(target, numbers):
    # Initially all numbers are candidates
    candidates = list(range(len(numbers)))
    #find_target_in_list(target, numbers, candidates)
