import knapsack

def test_normal_case_two_numbers():
    numbers = [1, 76, 28, 39, 6, 90]
    target = 96
    expected_solution = [4, 5]
    solution = knapsack.solve(target, numbers)

    assert solution == expected_solution
