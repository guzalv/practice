#!/usr/bin/env python3

import longest_substring

solution = longest_substring.Solution()

def test_longest_substring_in_the_middle():
    string = 'ffabczz'
    expected_result = 5 # Longest substring = 'fabcz'
    actual_result = solution.lengthOfLongestSubstring(string)

    assert actual_result == expected_result

def test_longest_substring_in_the_beggining():
    string = 'abczz'
    expected_result = 4 # Longest substring = 'abcz'
    actual_result = solution.lengthOfLongestSubstring(string)

    assert actual_result == expected_result

def test_longest_substring_in_the_end():
    string = 'ffabc'
    expected_result = 4 # Longest substring = 'fabc'
    actual_result = solution.lengthOfLongestSubstring(string)

    assert actual_result == expected_result

def test_two_substrings_same_length():
    string = 'ffabccbazz'
    expected_result = 4 # Longest substring = 'fabc' or 'cbaz'
    actual_result = solution.lengthOfLongestSubstring(string)

    assert actual_result == expected_result

def test_all_characters_are_the_same():
    string = 'aaaaaaaa'
    expected_result = 1 # Longest substring = 'a'
    actual_result = solution.lengthOfLongestSubstring(string)

    assert actual_result == expected_result

def test_empty_string():
    string = ''
    expected_result = 0 # Longest substring = ''
    actual_result = solution.lengthOfLongestSubstring(string)

    assert actual_result == expected_result

def test_leetcode_failing_1():
    string = 'dvdf'
    expected_result = 3 # Longest substring = 'vdf'
    actual_result = solution.lengthOfLongestSubstring(string)

    assert actual_result == expected_result
