#!/usr/bin/env python3

class Solution(object):

    def lengthOfLongestSubstring(self, s):
        longest_substring = ''
        current_substring = ''

        # "Main" loop
        for letter in s:
            # If this letter is in the current substring, the substring ended
            # at the previous character.
            letter_pos = current_substring.find(letter)
            if letter_pos >= 0:
                # If it is longer than the current longest, this is the new
                # longest.
                if len(current_substring) > len(longest_substring):
                    longest_substring = current_substring

                # Start a new current_substring from the letter next to the
                # last occurence of the current one
                current_substring = (current_substring[letter_pos + 1 :] +
                    letter)

            # Otherwise add it to the current substring
            else:
                current_substring += letter

        # After we have gone through the string, re-check if the current
        # substring is longer than the longest
        if len(current_substring) > len(longest_substring):
            longest_substring = current_substring

        return len(longest_substring)
